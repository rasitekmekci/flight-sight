FROM node:12-alpine as builder
WORKDIR /app
COPY . .
RUN npm set audit false && npm install
RUN npm run build
#

FROM mhart/alpine-node
RUN yarn global add serve
WORKDIR /app
COPY --from=builder /app/build .
CMD ["serve", "-p", "80", "-s", "."]
