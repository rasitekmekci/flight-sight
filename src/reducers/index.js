import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import flightReducer from './flights';

export default (history) => combineReducers({
  router: connectRouter(history),
  flight: flightReducer
});
