import {
  actions,
  REQUEST_FLIGHTS_BUSINESS,
  REQUEST_FLIGHTS_CHEAP,
  RECEIVE_FLIGHTS_BUSINESS,
  RECEIVE_FLIGHTS_CHEAP
} from "../actions";

const initialState = {
  cheap: [],
  business: []
};

const flightReducer = (state = initialState, action) => {
  switch (action.type) {
    case RECEIVE_FLIGHTS_BUSINESS:

      return {
        ...state,
        business: action.payload
      };

    case RECEIVE_FLIGHTS_CHEAP:
      const flights = action.payload.map(flight => {
        const [departure, arrival] = flight.route.split('-');

        return {
          departure,
          arrival,
          departureTime: flight.departure,
          arrivalTime: flight.arrival,
        }
      });
      return {
        ...state,
        cheap: flights
      };
    default:
      return state
  }
};

export default flightReducer;
