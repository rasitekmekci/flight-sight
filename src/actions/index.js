export const REQUEST_FLIGHTS_CHEAP = 'flights/REQUEST_CHEAP';
export const RECEIVE_FLIGHTS_CHEAP = 'flights/RECEIVE_CHEAP';
export const FETCH_FLIGHTS_CHEAP = 'flights/FETCH_CHEAP';

export const REQUEST_FLIGHTS_BUSINESS = 'flights/REQUEST_BUSINESS';
export const RECEIVE_FLIGHTS_BUSINESS = 'flights/RECEIVE_BUSINESS';
export const FETCH_FLIGHTS_BUSINESS = 'flights/FETCH_BUSINESS';

export const actions = {
  // Cheap Flights
  requestCheapFlights: () => {
    return { type: REQUEST_FLIGHTS_CHEAP };
  },
  requestCheapFlightsSuccess: data => {
    return { type: RECEIVE_FLIGHTS_CHEAP, payload: data };
  },
  fetchCheapFlights: () => {
    return { type: FETCH_FLIGHTS_CHEAP };
  },

  // Business Flights
  requestBusinessFlights: () => {
    return { type: REQUEST_FLIGHTS_BUSINESS };
  },
  requestBusinessFlightsSuccess: data => {
    return { type: RECEIVE_FLIGHTS_BUSINESS, payload: data };
  },
  fetchBusinessFlights: () => {
    return { type: FETCH_FLIGHTS_BUSINESS };
  },

};
