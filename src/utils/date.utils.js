import { format, parse } from 'date-fns';

// convert date in mili
export function fromSeconds(timestamp) {
  return parse(timestamp, 't', new Date());
}

export function defaultDateFormat(date) {
  return format(date, "yyyy-MM-dd hh:mm");
}
