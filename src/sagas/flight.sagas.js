import { put, call, takeEvery } from "redux-saga/effects";
import { actions as flightActions } from "../actions";

import { push } from "connected-react-router";

const { REACT_APP_API_BASE_URL: baseURL } = process.env;

export function* fetchBusinessFlightsAsync() {
  try {
    yield put(flightActions.requestBusinessFlights());
    const { data } = yield call(() => {
      return fetch(`${baseURL}/business`).then(res =>
        res.json()
      );
    });
    yield put(flightActions.requestBusinessFlightsSuccess(data));
  } catch (error) {
    // yield put(flightActions.requestBusinessFlightsError());
    yield put(push("/"));
  }
}

export function* fetchCheapFlightsAsync() {
  try {
    yield put(flightActions.requestCheapFlights());
    const { data } = yield call(() => {
      return fetch(`${baseURL}/cheap`).then(res =>
        res.json()
      );
    });

    yield put(flightActions.requestCheapFlightsSuccess(data));
  } catch (error) {
    console.log('error:', error);
    // TODO implement error action and flow
    // yield put(push("/"));
  }
}
