import { takeEvery, all } from "redux-saga/effects";
import * as flightSagas from "./flight.sagas";

import { FETCH_FLIGHTS_BUSINESS, FETCH_FLIGHTS_CHEAP } from "../actions";

// Sagas
export function* watchCheapFlightRequests() {
  yield takeEvery(FETCH_FLIGHTS_CHEAP, flightSagas.fetchCheapFlightsAsync);
}

export function* watchCheapFlightResponse() {
  yield takeEvery(FETCH_FLIGHTS_CHEAP, flightSagas.fetchCheapFlightsAsync);
}

export function* watchBusinessFlightRequests() {
  yield takeEvery(FETCH_FLIGHTS_BUSINESS, flightSagas.fetchBusinessFlightsAsync);
}

export default function* rootSaga() {
  yield all([
    watchCheapFlightRequests(),
    watchBusinessFlightRequests()
  ]);
}
