import React from 'react';
import { connect } from 'react-redux'

import { Button, Icon, Input, Table } from 'antd';
import Highlighter from 'react-highlight-words';


import { actions } from 'actions';
import { defaultDateFormat, fromSeconds } from "../../utils/date.utils";

function parseDate(date) {
  return defaultDateFormat(fromSeconds(date));
}


// A quick hack to bypass duplicate list item unique key issue.
let uniqueId = 0;

class List extends React.Component {
  state = {
    cheap: [],
    business: [],
    flights: [],
    searchText: '',
    filteredInfo: null,
  };

  handleSearch = (selectedKeys, confirm) => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  };

  handleReset = clearFilters => {
    clearFilters();
    this.setState({ searchText: '' });
  };

  getColumnSearchProps = dataIndex => ({
    filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={node => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
          onPressEnter={() => this.handleSearch(selectedKeys, confirm)}
          style={{ width: 188, marginBottom: 8, display: 'block' }}
        />
        <Button
          type="primary"
          onClick={() => this.handleSearch(selectedKeys, confirm)}
          icon="search"
          size="small"
          style={{ width: 90, marginRight: 8 }}
        >
          Search
        </Button>
        <Button onClick={() => this.handleReset(clearFilters)} size="small" style={{ width: 90 }}>
          Reset
        </Button>
      </div>
    ),
    filterIcon: filtered => (
      <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        .toString()
        .toLowerCase()
        .includes(value.toLowerCase()),
    onFilterDropdownVisibleChange: visible => {
      if (visible) {
        setTimeout(() => this.searchInput.select());
      }
    },
    render: text => {
      if (!text) {
        return null;
      }
      return (
        <Highlighter
          highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text.toString()}
        />
      )
    },
  });

  handleChange = (pagination, filters, sorter) => {
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter,
    });
  };

  clearFilters = () => {
    this.setState({ filteredInfo: null });
  };

  clearAll = () => {
    this.setState({
      filteredInfo: null,
      sortedInfo: null,
    });
  };

  componentDidMount() {
    this.props.dispatch(actions.fetchCheapFlights());
    this.props.dispatch(actions.fetchBusinessFlights());
  }

  componentDidUpdate(prevProps, prevState, snapshot) {

    if (this.props.cheap !== prevProps.cheap) {
      const cheap = this.generateTableData(this.props.cheap, 'cheap');
      this.setState((state, props) => ({
        cheap,
        flights: [...state.flights, ...cheap]
      }));
    }

    if (this.props.business !== prevProps.business) {
      const business = this.generateTableData(this.props.business, 'business');
      this.setState((state, props) => ({
       business,
       flights: [...state.flights, ...business]
      }));
    }
  }

  generateTableData(requestData, flightType) {
    return requestData.map(item => ({
     ...item,
      type: flightType
    }));

  }

  render() {
    let { sortedInfo, filteredInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};

    console.log('state');
    console.log(this.state.flights);

    const columns = [
      {
        title: 'Type',
        dataIndex: 'type',
        width: 100,
        filters: [
          { text: 'Business', value: 'business' },
          { text: 'Economic', value: 'cheap' }
        ],
        filteredValue: filteredInfo.type || null,
        onFilter: (value, record) => record.type === value,
      },
      {
        title: 'Departure',
        dataIndex: 'departure',
        width: 250,
        ...this.getColumnSearchProps('departure'),
      },
      {
        title: 'Arrival',
        dataIndex: 'arrival',
        width: 250,
        ...this.getColumnSearchProps('arrival'),
      },
      {
        title: 'Departure Time',
        dataIndex: 'departureTime',
        width: 250,
        render: dateString => `${parseDate(dateString)}`,
        sorter: (a, b) => a.departureTime - b.departureTime,
        sortOrder: sortedInfo.columnKey === 'departureTime' && sortedInfo.order,
      },
      {
        title: 'Arrival Time',
        dataIndex: 'arrivalTime',
        width: 250,
        render: dateString => `${parseDate(dateString)}`,
        sorter: (a, b) => a.arrivalTime - b.arrivalTime,
        sortOrder: sortedInfo.columnKey === 'arrivalTime' && sortedInfo.order,
      }
    ];

    return (
      <div>
        List Page

        <Table columns={columns} dataSource={this.state.flights}
               pagination={{ pageSize: 10 }} scroll={{ y: 400 }}
               onChange={this.handleChange}
               rowKey={(record) => {
                 if (!record.__uniqueId)
                   record.__uniqueId = ++uniqueId;
                 return record.__uniqueId;
               }}/>,
      </div>
    );
  }
}


function mapStateToProps(state) {
  const { flight } = state;
  return {
    cheap: flight.cheap,
    business: flight.business
  }
}

export default connect(mapStateToProps)(List);
